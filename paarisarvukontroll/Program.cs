﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace paarisarvukontroll
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("anna üks arv: "); // kirjutab ekraanile "anna üks arv " ja küsib numbri
  
            if (int.TryParse(Console.ReadLine(), out int arv)) // määrab ära kirjutatud rea pealt, et sisestatud arv on muutuja "arv"
            { 
          
                if (arv % 2 == 0) // arvutab tehte "arv" jagatud kahega peab võrduma nulliga
                {
                Console.WriteLine($"sinu arv {arv} on paarisarv"); // kui arvutuse vastus on tõene, siis on paarisarv
                }
                else
                {
                Console.WriteLine($"sinu arv {arv} ei ole paarisarv"); // kui vastus on väär, siis on arv paaritu
                
                       Console.WriteLine($"sinu {arv} on { (arv % 2 == 0 ? "paaris" : "paaritu" ) } "); //millegipärast arvas ta,  et paaritu esimene " lõpetab
                }
            }
        }
    }
}       
